set nocompatible

set number " relativenumber
set noswapfile
set ignorecase
set incsearch
set hlsearch
set viminfo='50,<1000,s100,:0,n~/.vim/viminfo
set autoindent
set textwidth=80

filetype plugin on

syntax on

map <C-o> :open .<CR>

map <C-l>   :tabn   <CR>
map <C-h>   :tabp   <CR>
map <C-n>   :tabnew <CR>
" map <C-w>   :close  <CR>
map <C-q>   :q      <CR>
map <C-s>   :w      <CR>

map <F2> :colorscheme delek<CR>
map <F3> :colorscheme morning<CR>

imap kj <ESC>
imap KJ <ESC>
imap <C-s> <ESC>:w<CR>a
nnoremap \\ :noh<cr>

autocmd FileType html setlocal et ts=2 sw=2
autocmd FileType markdown setlocal et ts=4 sw=4
autocmd FileType go   setlocal noet ts=4 sw=4
autocmd BufNewFile *.go 0r ~/.vim/skeletons/go.go
autocmd BufNewFile *.go normal GddO
autocmd BufNewFile *.go startinsert
colorscheme delek
