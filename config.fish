export GOPATH="/home/$USER/.config/go"
export GOPROXY=direct
export GOSUMDB=off

export NODE_REPL_HISTORY=""

alias xs='xbps-query -Rs'
alias xi='sudo xbps-install -u'
alias xu='sudo xbps-install -Su'
alias xr='sudo xbps-remove -R'

alias dns='dnf -C search'
alias dni='sudo dnf install'
alias dnic='sudo dnf -C install'
alias dnr='sudo dnf remove'
alias dnu='sudo dnf upgrade'
alias dnc='sudo dnf makecache'

alias aps='apt search'
alias api='sudo apt install'
alias apu='sudo apt update'
alias app='sudo apt purge'
alias apr='sudo apt autoremove'
alias apug='sudo apt update && sudo apt upgrade'

alias pi='sudo pacman -S'
alias pp='sudo pacman -Rns'
alias pu='sudo pacman -Syu'
alias pup='sudo pacman -Sy'
alias psr='pacman -Ss'

alias v='vim'
alias sr='cd ~/src'
alias ls='ls -l -A -h --color=auto' # -h and --color aren't POSIX-compliant.
